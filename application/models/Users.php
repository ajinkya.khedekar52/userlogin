<?php

class Users extends CI_Model {
	public function check_mail($email) {
		$this->db->where('email',$email);
		return $this->db->get('users')->row();
	}

	public function save_user($pass) {
		$data = ['first_name' => $this->input->post('fname'), 'last_name' => $this->input->post('lname'), 'email' => $this->input->post('email'), 'password' => $pass];
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function get_info() {
		return $this->db->get('users')->result();
	}

	public function get_dept() {
		return $this->db->get('department');
	}

	public function get_details() {
		$this->db->select('users.*, department.department_name, department.id as dept_id, sub_department.sub_department, sub_department.id as sub_dept_id');
		$this->db->from('users');
		$this->db->join('department','users.dept_id = department.id','left');
		$this->db->join('sub_department', 'users.sub_dept_id = sub_department.id','left');
		return $this->db->get()->result();
	}

	public function get_departments() {
		return $this->db->get('department')->result();
	}

	public function get_sub_departments($dept) {
		$this->db->where('dept_id', $dept);
		return $this->db->get('sub_department')->result();
	}

	public function get_user($id) {
		$this->db->where('id', $id);
		return $this->db->get('users')->row();
	}

	public function update_details($db_array, $id) {
		$this->db->where('id', $id);
		$this->db->update('users', $db_array);
		return true;
	}

	public function auth_user($email, $password) {
		$pass = hash('sha512', $password);
		$this->db->where('email', $email);
		$this->db->where('password', $pass);
		return $this->db->get('users');
	}
}

?>