<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('Users');
      }

	public function index()
	{
		$data['main_content'] = 'Users/login';
		$this->load->view('Users/includes/template', $data);
	}

	public function check_email() {
		$email = $this->input->post('email');
		if ($this->input->post('password') != "") {
			$password = $this->input->post('password');
			$auth = $this->Users->auth_user($email, $password);
			if ($auth->num_rows() == 0) {
				$this->session->set_flashdata('err', 'User Credentials Are Wrong.');
				redirect('User/index');
			} else {
				$user = $auth->row();
				$user_data = ['user_id' => $user->id, 'email' => $user->email, 'firstname' => $user->first_name];
				$this->session->set_userdata($user_data);
				redirect('User/dashboard');
			}
		} else {
			$email_data = $this->Users->check_mail($email);
			if (!empty($email_data)) {
				$data['email'] = $email_data->email;
				$data['main_content'] = 'Users/login';
				$this->load->view('Users/includes/template', $data);
			} else {
				$base_64 = base64_encode($email);
				$url_param = rtrim($base_64, '=');
				redirect('User/sign_up/'.$url_param);
			}
		}
	}

	public function sign_up($url_param) {
			$base_64 = $url_param . str_repeat('=', strlen($url_param) % 4);
			$email = base64_decode($base_64);
			$data['email'] = $email;
			$data['main_content'] = 'Users/signup';
			$this->load->view('Users/includes/template', $data);
	}

	public function save_user() {
		$pass = hash('sha512', $this->input->post('password'));
		$check = $this->Users->save_user($pass);
		if (!empty($check)) {
			$user_data = ['user_id' => $check, 'email' => $this->input->post('email'), 'firstname' => $this->input->post('fname')];
			$this->session->set_userdata($user_data);
			redirect('User/dashboard');
		}
	}

	public function dashboard() {
		if ($this->session->userdata('user_id')) {
			$data['main_content'] = 'Users/dashboard';
			$this->load->view('Users/includes/template', $data);
		} else {
			redirect(base_url());
		}
	}

	public function get_details() {
		$data = $this->Users->get_details();
		if (!empty($data)) {
			echo json_encode($data);
		}
	}

	public function get_departments() {
		$dept = $this->Users->get_departments();
		if (!empty($dept)) {
			echo json_encode($dept);
		}
	}

	public function get_sub_departments() {
		$dept = $this->input->post('dept');
		$sub_dept = $this->Users->get_sub_departments($dept);
		if (!empty($sub_dept)) {
			echo json_encode($sub_dept);
		}
	}

	public function get_user() {
		$id = $this->input->post('id');
		$users = $this->Users->get_user($id);
		if (!empty($users)) {
			echo json_encode($users);
		}
	}

	public function update_user() {
		$url = realpath(APPPATH.'../assets/uploads');
		
		$config['upload_path'] = $url;
	    $config['allowed_types'] = 'jpg|png';

	    $this->load->library('upload', $config);

	    if (!$this->upload->do_upload('profile')) {
	        $error = array('error' => $this->upload->display_errors());
	        echo json_encode(['status' => false, 'message' => $error['error']]);
	    } else {
	        $data = $this->upload->data();
	        $db_array['profile_pic'] = $data['file_name'];
	    }

	    $db_array['dept_id'] = $this->input->post('department');
	    $db_array['sub_dept_id'] = $this->input->post('sub_department');
	    $db_array['status'] = 2;

	    $update_details = $this->Users->update_details($db_array, $this->input->post('valueId'));
	    if ($update_details) {
	    	echo json_encode(['status' => true, 'message' => 'Details Updated']);	
	    } else {
	    	echo json_encode(['status' => false, 'message' => 'Some error occured']);
	    }
	    
	}

	public function logout() {
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'user_id' && $key != 'email' && $key != 'firstname') {
                $this->session->unset_userdata($key);
            }
        }
    	$this->session->sess_destroy();
    	redirect(base_url());
	}
}
