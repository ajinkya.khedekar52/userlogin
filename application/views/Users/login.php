<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
    <div class="container">
        <?php if ($this->session->flashdata('err')) {?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('err'); ?>
        </div>
        <?php }?>
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="<?php echo base_url();?>User/check_email" method="post">
                        <h3 class="text-center text-info">Login</h3>
                        <div class="form-group">
                            <label for="email" class="text-info">Email:</label><br>
                            <input type="email" name="email" id="email" class="form-control" value="<?php if (isset($email)) { echo $email; } ?>" <?php if (isset($email)) { echo "readonly"; } ?> required>
                        </div>
                        <?php if (isset($email)) {?>
                        <div class="form-group" id="" style="">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit" id="submit_form">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>