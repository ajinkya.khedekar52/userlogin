<div id="login">
    <h3 class="text-center text-white pt-5">Dashboard</h3>
    <div class="container">
        <a href="<?php echo base_url();?>User/logout"><button class="btn btn-primary mb-3 float-right">Logout</button></a><br><br>
        <div id="login-row" class="row justify-content-center align-items-center">
            <table class="table table-border">
                <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Profile Picture</th>
                        <th>Department</th>
                        <th>Sub Department</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="table_body">
                    
                </tbody>
            </table>
        </div>
    </div>
</div>