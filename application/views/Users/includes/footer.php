<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="update-form" class="form" action="<?php echo base_url();?>User/update_user" method="post">
                        <div class="form-group">
                            <label for="fname" class="text-info">Firstname:</label><br>
                            <input type="text" name="fname" id="fname" class="form-control" readonly required>
                            <input type="hidden" name="valueId" id="valueId">
                        </div>
                        <div class="form-group">
                            <label for="lname" class="text-info">Lastname:</label><br>
                            <input type="text" name="lname" id="lname" class="form-control" readonly required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="text-info">Email:</label><br>
                            <input type="email" name="email" id="email" class="form-control" readonly value="">
                        </div>
                        <div class="form-group">
                            <label for="profile_pic" class="text-info">Profile Pic:</label><br>
                            <input type="file" name="profile" id="profile_pic" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="department" class="text-info">Department:</label><br>
                            <select class="form-control" id="department" name="department" required>
                            	
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sub_department" class="text-info">Sub Department:</label><br>
                            <select class="form-control" id="sub_department" name="sub_department" required>
                            	
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit" id="submit_form">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var base_url = '<?php echo base_url();?>';
		if ($("#table_body").length) {
			display_table(base_url);
		}

		$("body").on('click', '.edt-btn', function() {
			$.ajax({
				url: base_url+"User/get_departments",
				type: "POST",
				success: function(response) {
					var html = "<option value=''>Select Department</option>";
					var data = JSON.parse(response);
					$.each(data, function(key, value) {
						html += "<option value="+value.id+">"+value.department_name+"</option>";
					})
					$("#department").html(html);
				}
			});
			var id = $(this).attr('id');
			$.ajax({
				url: base_url+"User/get_user",
				type: "POST",
				data: {id: id},
				success: function(response) {
					var data = JSON.parse(response);
					$("#fname").val(data.first_name);
					$("#lname").val(data.last_name);
					$("#email").val(data.email);
					$("#valueId").val(data.id);
				}
			})
			$("#myModal").modal('show');
		});

		$("body").on('change', "#department", function() {
			var dept = $(this).val();
			$.ajax({
				url: base_url+"User/get_sub_departments",
				type: "POST",
				data:{dept: dept},
				success: function(response) {
					var html = "<option value=''>Select Sub Department</option>";
					var data = JSON.parse(response);
					$.each(data, function(key, value) {
						html += "<option value="+value.id+">"+value.sub_department+"</option>";
					})
					$("#sub_department").html(html);
				}
			})
		})

		$("#update-form").submit(function(e) {
			e.preventDefault();
			var form = $("#update-form")[0];
			var formData = new FormData(form);
			$.ajax({
				url: base_url+'User/update_user',
				type: "POST",
				data: formData,
				contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    			processData: false,
				success: function(response) {
					var data = JSON.parse(response);
					if (data.status == true) {
						alert(data.message);
						setTimeout(function() {$('#myModal').modal('hide');}, 2000);
						display_table(base_url);
					} else {
						alert(data.message);
					}
				}
			})
		})
	})
	function display_table(base_url) {
		$.ajax({
			url: base_url+'User/get_details',
			type: 'POST',
			success: function(response) {
				console.log(response);
				var data = JSON.parse(response);
				var html = "";
				$.each(data, function(key, value) {
					console.log(value);
					html += "<tr><td>"+value.first_name+"</td><td>"+value.last_name+"</td><td>"+value.email+"</td><td><img src="+base_url+"assets/uploads/"+value.profile_pic+" width='150' height='150' /></td><td>"+ ((value.department_name == null) ? '' : value.department_name) +"</td><td>"+ ((value.sub_department == null) ? '' : value.sub_department)+"</td><td><button class='btn btn-sm btn-primary edt-btn' id="+value.id+" style='"+((value.status == 2) ? "display: none" : '')+"'>Add Details</button></td>";
				})
				$("#table_body").html(html);
			}
		})
	}
</script>
</html>