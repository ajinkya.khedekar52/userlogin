<div id="login">
    <h3 class="text-center text-white pt-5">Signup form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="<?php echo base_url();?>User/save_user" method="post">
                        <h3 class="text-center text-info">Sign Up</h3>
                        <div class="form-group">
                            <label for="fname" class="text-info">Firstname:</label><br>
                            <input type="text" name="fname" id="fname" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="text-info">Lastname:</label><br>
                            <input type="text" name="lname" id="lname" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email" class="text-info">Email:</label><br>
                            <input type="email" name="email" id="email" class="form-control" readonly value="<?php if (isset($email)) { echo $email; }?>">
                        </div>
                        <div class="form-group" id="" style="">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit" id="submit_form">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>